using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draft : MonoBehaviour
{

    private Ray ray;
    private RaycastHit hit;
    private Vector3 rot = new Vector3(0, 0, 0);

    void Update()
    {

        if (Input.GetMouseButton(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                rot.z = hit.point.z;
                rot.x = hit.point.x;
                transform.position = new Vector3(rot.x, 0, 0);


            }
        }
    }
}