using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class MoveWheel : MonoBehaviour
{

    public LeanSelectableDial wheelJoystick;
    [SerializeField]
    private Rigidbody _rigidbody;
    [SerializeField]
    private float speedRotate = 0f;
    private float _curentPositionObject;
    private float deltaRotation;
    private float lastEulerJoystick;
    [SerializeField]
    private float addtorqueforce = 0f;
    [SerializeField]
    private float addtorquevelocity = 0f;
    [SerializeField]
    private float addtorqueacceleration = 0f;
    [SerializeField]
    private float addtorqueimpulse = 0f;
    [SerializeField]
    private float addforceforce = 0f;
    [SerializeField]
    private float _speedTimeDeltaTime = 0f;
    private float timeSpeed;





    // Start is called before the first frame update
    void Start()
    {
        lastEulerJoystick = wheelJoystick.Angle;

        {
            _rigidbody = gameObject.GetComponent<Rigidbody>();
        }


        // Update is called once per frame

    }




    void Update()
    {
        // _speed += _increaseSpeed * Time.deltaTime;



        float angleRotationDelta = wheelJoystick.Angle - lastEulerJoystick;
        var speed = _rigidbody.velocity.magnitude + speedRotate;
        //transform.Rotate(new Vector3(angleRotationDelta, 0, 0));

        // Quaternion deltaRotation = Quaternion.Euler(new Vector3(angleRotationDelta , 0, 0) * Time.deltaTime);
   
        // _rigidbody.MoveRotation(_rigidbody.rotation * deltaRotation); 
        _rigidbody.AddTorque(new Vector3(angleRotationDelta * addtorqueacceleration, 0, 0), ForceMode.Acceleration);
        _rigidbody.AddTorque(new Vector3(angleRotationDelta * speedRotate * speed , 0, 0));
        _rigidbody.AddTorque(new Vector3(angleRotationDelta * addtorqueforce, 0, 0), ForceMode.Force);
        _rigidbody.AddTorque(new Vector3(angleRotationDelta * addtorquevelocity, 0, 0), ForceMode.VelocityChange);
        _rigidbody.AddTorque(new Vector3(angleRotationDelta * addtorqueimpulse, 0, 0), ForceMode.Impulse);
        _rigidbody.AddForce(new Vector3(0, 0, angleRotationDelta * addforceforce));


        lastEulerJoystick = wheelJoystick.Angle;



    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("tiles"))
        {
            this.transform.parent = collision.transform;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.Equals("tiles"))
        {
            this.transform.parent = null;
        }
    }
}
