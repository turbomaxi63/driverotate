using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveingPlatform : MonoBehaviour
{
    float dirZ;
    float speed = 3f;
    bool moveingRight = true;

    void Update()
    {
        if (transform.position.z > 62f)
        {
            moveingRight = false;
        }
        else if (transform.position.z < 33.2f)
        {
            moveingRight = true;
        }
        if (moveingRight)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed*Time.deltaTime);

        }
        else 
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - speed * Time.deltaTime);
        }
    }
}
