using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriveWheel : MonoBehaviour
{
    public float speedRotateWheel;
    private Vector3 moveVector;
    [SerializeField]
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        

    }
    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space))
        {           
            rb.AddTorque(speedRotateWheel * Time.deltaTime, 0f, 0f);
        }
    }
}
