using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FunnyBlox
{
    public class Joystick : MonoBehaviour
    {
        private Canvas _canvas;
        private Camera _camera;
        private RectTransform _baseRect;

        [SerializeField]
        private bool _autoHide;

        [SerializeField]
        private bool _useTouchPosition;

        [SerializeField]
        private RectTransform _2DJoystick;

        [SerializeField]
        private Transform _3DJoystick;

        private Vector3 _touchPoint;
        private Vector3 _direction;
        private float _angle;
        private float _smoothAngle;

        [SerializeField]
        private float _smoothDuration = 0.5f;


        void OnEnable()
        {
            //����������� �� ������� �����
            EventsCotroller.OnInputAction += InputAction;
        }

        void OnDisable()
        {
            //���������� �� ������� �����
            EventsCotroller.OnInputAction -= InputAction;
        }

        void Start()
        {
            _baseRect = GetComponent<RectTransform>();
            _canvas = GetComponentInParent<Canvas>();
            _camera = _canvas.worldCamera;
            if (_2DJoystick && _autoHide)
                _2DJoystick.gameObject.SetActive(false);
        }

        private void InputAction(EInputAction action, Vector2 value)
        {
            switch (action)
            {
                //��������
                case EInputAction.Swipe:
                    if (_touchPoint.magnitude != value.magnitude)
                    {
                        _direction = (Vector3)value - _touchPoint;
                        _angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg - 90f;
                        _smoothAngle = Mathf.Lerp(_smoothAngle, _angle, _smoothDuration);
                        if (_2DJoystick)
                            _2DJoystick.rotation = Quaternion.AngleAxis(_smoothAngle, Vector3.forward);
                        if (_3DJoystick)
                            _3DJoystick.localRotation = Quaternion.AngleAxis(_smoothAngle, Vector3.forward);
                    }
                    break;
                //�������
                case EInputAction.Touch:
                    _touchPoint = value;

                    if (_2DJoystick)
                    {
                        if (_useTouchPosition)
                            _2DJoystick.anchoredPosition = ScreenPointToAnchoredPosition(value);
                        _2DJoystick.rotation = Quaternion.identity;

                        if (_autoHide)
                            _2DJoystick.gameObject.SetActive(true);
                    }

                    if (_3DJoystick)
                        _3DJoystick.localRotation = Quaternion.identity;

                    _direction = (Vector3)value - _touchPoint;
                    _angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg - 90f;
                    _smoothAngle = _angle;
                    break;
                //��������� ����
                case EInputAction.None:
                    if (_2DJoystick && _autoHide)
                        _2DJoystick.gameObject.SetActive(false);
                    if (_smoothAngle != _angle)
                        StartCoroutine(SmoothAngle());
                    break;
            }
        }

        private IEnumerator SmoothAngle()
        {
            while (_smoothAngle != _angle)
            {
                _smoothAngle = Mathf.Lerp(_smoothAngle, _angle, 0.5f);
                _2DJoystick.rotation = Quaternion.AngleAxis(_smoothAngle, Vector3.forward);
                if (_3DJoystick)
                    _3DJoystick.localRotation = Quaternion.AngleAxis(_smoothAngle, Vector3.forward);

                yield return new WaitForEndOfFrame();
            }
        }

        protected Vector2 ScreenPointToAnchoredPosition(Vector2 screenPosition)
        {
            Vector2 localPoint = Vector2.zero;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_baseRect, screenPosition, _camera, out localPoint))
            {
                Vector2 pivotOffset = _baseRect.pivot * _baseRect.sizeDelta;
                return localPoint - (_2DJoystick.anchorMax * _baseRect.sizeDelta) + pivotOffset;
            }
            return Vector2.zero;
        }

    }
}