using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HedgehogTeam.EasyTouch;

namespace FunnyBlox
{
    public class InputController : MonoBehaviour
    {
        private EasyTouch.SwipeDirection _lastSwipeDirection;

        private PlayerController _playerController;

        private bool _inRotate;

        void OnEnable()
        {
            EasyTouch.On_TouchStart += TouchStart;
            EasyTouch.On_TouchDown += TouchDown;
            EasyTouch.On_TouchUp += TouchUp;
        }

        void OnDisable()
        {
            EasyTouch.On_TouchStart -= TouchStart;
            EasyTouch.On_TouchDown -= TouchDown;
            EasyTouch.On_TouchUp -= TouchUp;
        }

        void Start()
        {
            _playerController = PlayerController.Instance;
        }

        void Update()
        {

        }

        private void TouchStart(Gesture gesture)
        {
            _lastSwipeDirection = EasyTouch.SwipeDirection.None;
            EventsCotroller.InputAction(EInputAction.Touch, gesture.position);
            _playerController.Breake();
            _inRotate = false;
        }

        private void TouchDown(Gesture gesture)
        {
            if (!_inRotate && gesture.swipe == EasyTouch.SwipeDirection.Other)
                _playerController.Breake();

            if (gesture.swipe != _lastSwipeDirection)
            {
                //�� �������
                if (gesture.swipe == EasyTouch.SwipeDirection.Left && _lastSwipeDirection == EasyTouch.SwipeDirection.DownLeft ||
                    gesture.swipe == EasyTouch.SwipeDirection.UpLeft && _lastSwipeDirection == EasyTouch.SwipeDirection.Left ||
                    gesture.swipe == EasyTouch.SwipeDirection.Up && _lastSwipeDirection == EasyTouch.SwipeDirection.UpLeft ||
                    gesture.swipe == EasyTouch.SwipeDirection.UpRight && _lastSwipeDirection == EasyTouch.SwipeDirection.Up ||
                    gesture.swipe == EasyTouch.SwipeDirection.Right && _lastSwipeDirection == EasyTouch.SwipeDirection.UpRight ||
                    gesture.swipe == EasyTouch.SwipeDirection.DownRight && _lastSwipeDirection == EasyTouch.SwipeDirection.Right ||
                    gesture.swipe == EasyTouch.SwipeDirection.Down && _lastSwipeDirection == EasyTouch.SwipeDirection.DownRight ||
                    gesture.swipe == EasyTouch.SwipeDirection.DownLeft && _lastSwipeDirection == EasyTouch.SwipeDirection.Down)
                {

                    if (!_inRotate)
                    {
                        _inRotate = true;
                        _playerController.ReleaseBreake();
                    }
                    _playerController.TurnPlayer(Mathf.Abs(gesture.deltaPosition.magnitude));
                }
                //������ �������
                else if (gesture.swipe == EasyTouch.SwipeDirection.Left && _lastSwipeDirection == EasyTouch.SwipeDirection.UpLeft ||
                        gesture.swipe == EasyTouch.SwipeDirection.DownLeft && _lastSwipeDirection == EasyTouch.SwipeDirection.Left ||
                        gesture.swipe == EasyTouch.SwipeDirection.Down && _lastSwipeDirection == EasyTouch.SwipeDirection.DownLeft ||
                        gesture.swipe == EasyTouch.SwipeDirection.DownRight && _lastSwipeDirection == EasyTouch.SwipeDirection.Down ||
                        gesture.swipe == EasyTouch.SwipeDirection.Right && _lastSwipeDirection == EasyTouch.SwipeDirection.DownRight ||
                        gesture.swipe == EasyTouch.SwipeDirection.UpRight && _lastSwipeDirection == EasyTouch.SwipeDirection.Right ||
                        gesture.swipe == EasyTouch.SwipeDirection.Up && _lastSwipeDirection == EasyTouch.SwipeDirection.UpRight ||
                        gesture.swipe == EasyTouch.SwipeDirection.UpLeft && _lastSwipeDirection == EasyTouch.SwipeDirection.Up)
                {
                    if (!_inRotate)
                    {
                        _inRotate = true;
                        _playerController.ReleaseBreake();
                    }
                    _playerController.TurnPlayer(-Mathf.Abs(gesture.deltaPosition.magnitude));
                }
            }


            _lastSwipeDirection = gesture.swipe;

            EventsCotroller.InputAction(EInputAction.Swipe, gesture.position);
        }

        private void TouchUp(Gesture gesture)
        {
            if (!_inRotate)
            {
                _inRotate = true;
                _playerController.ReleaseBreake();
            }
            EventsCotroller.InputAction(EInputAction.None, Vector2.zero);
        }
    }
}