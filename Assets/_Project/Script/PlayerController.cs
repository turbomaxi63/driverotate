using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FunnyBlox
{
    public class PlayerController : MonoSingleton<PlayerController>
    {
        [SerializeField]
        private Settings _settings;

        [LabelText("������������ AddForce, ����� AddTorque")]
        [SuffixLabel("_useForce", Overlay = true)]
        [SerializeField]
        private bool _useAddForce = true;

        [LabelText("����������� �������� ��� ����������")]
        [SuffixLabel("_useFreezeRotation", Overlay = true)]
        [SerializeField]
        private bool _useFreezeRotation = false;

        /// <summary>
        /// ������������ ���������
        /// </summary>
        private Transform _transform;
        /// <summary>
        /// ������������ ���������
        /// </summary>
        private Rigidbody _rigidbody;

        /// <summary>
        /// ������ ����������� ������
        /// </summary>
        private Vector3 _directionVector;

        private void Start()
        {
            _transform = transform;
            _rigidbody = _transform.GetComponent<Rigidbody>();
            if (_useAddForce)
                _directionVector = Vector3.forward;
            else
                _directionVector = Vector3.right;

        }

        /// <summary>
        /// ������� ������
        /// </summary>
        /// <param name="value">���� ������</param>
        public void TurnPlayer(float value)
        {
            value *= -_settings._addforceMultiplier;
            //����������� �������� ���� ��������� ������������
            if (value > 0f && value > _settings._addforceMax)
            {
                //Debug.Log("Forward");
                value = _settings._addforceMax;
            }
            else if (value < 0 && value < -_settings._addforceMax)
            {
                //Debug.Log("Backward");
                value = -_settings._addforceMax;
            }

            if (_useFreezeRotation && value != 0f)
                ReleaseBreake();

            if (_useAddForce)
                _rigidbody.AddForce(_directionVector * value);
            else
                _rigidbody.AddTorque(_directionVector * value);
        }

        public void Breake()
        {
            if (!_useFreezeRotation) return;
            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.constraints = (RigidbodyConstraints)(112 + 2);
        }

        public void ReleaseBreake()
        {
            if (!_useFreezeRotation) return;
            _rigidbody.constraints = (RigidbodyConstraints)(2 + 32 + 64);
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name.Equals("tiles"))
            {
                this.transform.parent = collision.transform;
            }
        }
        private void OnCollisionExit(Collision collision)
        {
            if (collision.gameObject.name.Equals("tiles"))
            {
                this.transform.parent = null;
            }
        }
    }
}