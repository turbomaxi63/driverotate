using Sirenix.OdinInspector;
using UnityEngine;

namespace FunnyBlox
{
    [CreateAssetMenu(fileName = "Settings", menuName = "Data/Settings", order = 51)]
    public class Settings : ScriptableObject
    {
        [FoldoutGroup("Настройки")]
        [LabelText("Множитель силы вращения")]
        [SuffixLabel("_addforceMultiplier", Overlay = true)]
        public float _addforceMultiplier = 1f;

        [FoldoutGroup("Настройки")]
        [LabelText("Максимальная сила толчка колеса")]
        [SuffixLabel("_addforceMax", Overlay = true)]
        public float _addforceMax = 100f;
    }
}