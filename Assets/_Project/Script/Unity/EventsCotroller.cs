﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FunnyBlox
{
    public class EventsCotroller
    {
        public static event Action<EGameState> OnChangeGameState;
        public static event Action<EInputAction, Vector2> OnInputAction;
        public static event Action<bool> OnResetGame;
        public static event Action OnStartGame;
        public static event Action<int> OnSwitchColor;

        public static void ChangeGameState(EGameState gameState)
        {
            OnChangeGameState(gameState);
        }

        /// <summary>
        /// Событие ввода манипулятором
        /// </summary>
        /// <param name="inputAction">тип события</param>
        /// <param name="direction">напрявление</param>
        public static void InputAction(EInputAction inputAction, Vector2 direction)
        {
            OnInputAction(inputAction, direction);
        }

        /// <summary>
        /// Старт игры
        /// </summary>
        public static void StartGame()
        {
            OnStartGame();
        }

        /// <summary>
        /// Рестарт игры
        /// </summary>
        /// <param name="state"></param>
        public static void ResetGame(bool state)
        {
            OnResetGame(state);
        }

        /// <summary>
        /// Переключение цвета
        /// </summary>
        /// <param name="number"></param>
        public static void SwitchColor(int number)
        {
            OnSwitchColor(number);
        }
    }
}