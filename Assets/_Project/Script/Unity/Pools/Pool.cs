﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool
{
    /// <summary>
    /// Список элементов пула
    /// </summary>
    public List<PoolElement> PoolElements { get; private set; }

    /// <summary>
    /// Название пула
    /// </summary>
    public string _name { get; private set; }
    /// <summary>
    /// Трансформ оригинал
    /// </summary>
    private Transform _elementTransform;
    /// <summary>
    /// Выключать объект после его создания в списке
    /// </summary>
    private bool _disableAfterCreate;
    /// <summary>
    /// Скрип-монобех на элементе
    /// </summary>
    private MonoBehaviour _monoBehaviour;
    /// <summary>
    /// Родитель элемента (корневой пул)
    /// </summary>
    private Transform _parentElements;
    /// <summary>
    /// хранилище кушированного класса
    /// </summary>
    private PoolElement _poolElement;

    /// <summary>
    /// Конструктор пула элементов
    /// </summary>
    /// <param name="element"></param>
    /// <param name="parent"></param>
    public Pool(Element element, Transform parent)
    {
        PoolElements = new List<PoolElement>(element._countElements);

        _name = element._name == "" ? element._transform.name : element._name;

        _elementTransform = element._transform;
        _disableAfterCreate = element._disabledAfterCreate;
        _monoBehaviour = element._monoBehaviour;

        _parentElements = parent;

        for (int i = 0; i < element._countElements; ++i)
        {
            InstantiateElement();
        }
    }

    /// <summary>
    /// Инстансим объект в сцену
    /// </summary>
    /// <returns></returns>
    private PoolElement InstantiateElement()
    {
        _poolElement = ScriptableObject.CreateInstance<PoolElement>();
        _poolElement._transform = Object.Instantiate(_elementTransform);
        _poolElement._transform.name = string.Format("{0}_{1:000}", _elementTransform.name, PoolElements.Count);
        _poolElement._transform.parent = _parentElements;

        if (_disableAfterCreate)
            _poolElement._transform.gameObject.SetActive(false);
        else
            _poolElement._transform.localPosition = new Vector3(1000f, 1000f, 0f);

        if (_monoBehaviour != null)
            _poolElement._script = _poolElement._transform.GetComponent<MonoBehaviour>();

        PoolElements.Add(_poolElement);

        return _poolElement;
    }

    /// <summary>
    /// Возвращает элемент из пула
    /// </summary>
    /// <returns></returns>
    public PoolElement GetElement()
    {
        _poolElement = PoolElements.Find(PoolElements => !PoolElements.IsBusy);

        if (_poolElement == null) _poolElement = InstantiateElement();

        _poolElement.IsBusy = true;

        if (_disableAfterCreate) _poolElement._transform.gameObject.SetActive(true);

        return _poolElement;
    }

    /// <summary>
    /// Возвращает элемент обратно в пул
    /// </summary>
    /// <param name="element">Трансформ</param>
    /// /// <param name="reposition">Убирать за пределы экрана</param>
    public void FreeElement(Transform element, bool reposition = false, bool disable = true)
    {
        _poolElement = PoolElements.Find(PoolElements => PoolElements._transform == element);

        if(_poolElement != null)
        {
            _poolElement._transform.parent = _parentElements;
            _poolElement.IsBusy = false;
            if (reposition) _poolElement._transform.localPosition = new Vector3(1000f, 1000f, 0f);
            if (_disableAfterCreate && disable) _poolElement._transform.gameObject.SetActive(false);
        }
    }
}
