﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Element : System.Object
{
    [Header("Название пула")]
    public string _name;
    [Header("Оригинальный трансформ")]
    public Transform _transform;
    [Header("Количество элементов в пуле")]
    public int _countElements;
    [Header("Отключать объекты после создания")]
    public bool _disabledAfterCreate;
    [Header("Скрипт который будет использоваться в элементе")]
    public MonoBehaviour _monoBehaviour;
    [Header("Парент для объектов пула")]
    public Transform _parent;

}


public class PoolCollection : MonoBehaviour
{

    [Header("Список пулов")]
    [SerializeField]
    private Element[] _poolElements;

    private static List<Pool> _pools;

    void Awake()
    {
        _pools = new List<Pool>();
        for (int i = 0, imax = _poolElements.Length; i < imax; i++)
        {
            CreatePool(i);
        }
    }

    public void CreatePool(int number)
    {
        Pool pool = new Pool(_poolElements[number], _poolElements[number]._parent == null ? transform : _poolElements[number]._parent);

        _pools.Add(pool);
    }

    public static List<Pool> GetPools()
    {
        return _pools;
    }

    public static Pool GetPool(int number)
    {
        return _pools[number];
    }

    public static Pool GetPool(string poolName)
    {
        try
        {
            return _pools.Find(_pools => _pools._name == poolName);
        }
        catch (Exception)
        {
            return null;
        }

    }
}