﻿using UnityEngine;
using System.Collections;

public class PoolElement : ScriptableObject
{

    public bool IsBusy { get; set; }

    public Transform _transform { get; set; }

    public MonoBehaviour _script { get; set; }


}
