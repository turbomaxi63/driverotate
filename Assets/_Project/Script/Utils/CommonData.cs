﻿using Sirenix.OdinInspector;

namespace FunnyBlox
{
    public enum EGameState
    {
        RunGame = 0,
        Menu = 1,
        Game = 2,
        GameWait = 3,
        GameOver = 5,
        GameWin = 6,
        NextLevel = 7,
        GameToReset = 8,
    }

    public enum EInputAction
    {
        None = 0,
        Touch = 1,
        LongTouch = 2,
        Vertical = 3,
        Horizontal = 4,
        Swipe = 5,
    }

    public class CommonData : MonoSingleton<CommonData>
    {
        //---------------------------------------------------//
        [FoldoutGroup("Данные для аппстора")]
        [InfoBox("Бандл нейм", InfoMessageType.None)]
        public string BUNDLE_IDENTIFIER_APPLE = "BUNDLE_NAME";

        [FoldoutGroup("Данные для аппстора")]
        [InfoBox("ИД приложения в аппсторе", InfoMessageType.None)]
        public string APPLE_ID = "EMPTY";

        [FoldoutGroup("Данные для аппстора")]
        [InfoBox("ИД инапа в аппсторе", InfoMessageType.None)]
        public string IAP_NO_ADS_PRODUCT_ID_APPLE = "EMPTY";

        [FoldoutGroup("Данные для аппстора")]
        [InfoBox("Ссылка в аппсторе", InfoMessageType.None)]
        public string APPLE_STORE = "EMPTY_URL";

        [FoldoutGroup("Данные для аппстора")]
        [InfoBox("лидерборд в аппсторе", InfoMessageType.None)]
        public string APPSTORE_LEADERBOARD = "EMPTY";

        [FoldoutGroup("Данные для аппстора")]
        [InfoBox("ИД приложений в аппсторе", InfoMessageType.None)]
        public string[] OTHER_APP_APPLE_ID;

        [FoldoutGroup("Данные для аппстора")]
        [InfoBox("Ссылки на приложения в аппсторе", InfoMessageType.None)]
        public string[] OTHER_APP_APPLE_URL;

        //-----------------------------------------------------//
        [FoldoutGroup("Данные для Гуглстора")]
        [InfoBox("Бандл нейм для гугла", InfoMessageType.None)]
        public string BUNDLE_IDENTIFIER_GOOGLE = "BUNDLE_NAME";

        [FoldoutGroup("Данные для Гуглстора")]
        [InfoBox("ИД приложения в гуглплее", InfoMessageType.None)]
        public string GOOGLEPLAY_ID = "EMPTY";

        [FoldoutGroup("Данные для Гуглстора")]
        [InfoBox("ИД инапа в гуглплее", InfoMessageType.None)]
        public string IAP_NO_ADS_PRODUCT_ID_GOOGLE = "EMPTY";

        [FoldoutGroup("Данные для Гуглстора")]
        [InfoBox("Ссылка в гуглплее", InfoMessageType.None)]
        public string GOOGLE_PLAY = "EMPTY_URL";

        [FoldoutGroup("Данные для Гуглстора")]
        [InfoBox("Лидерборд в гуглплее", InfoMessageType.None)]
        public string GOOPLEPLAY_LEADERBOARD = "EMPRY";

        [FoldoutGroup("Данные для Гуглстора")]
        [InfoBox("ИД приложений в гуглплее", InfoMessageType.None)]
        public string[] OTHER_APP_GOOGLE_ID;

        [FoldoutGroup("Данные для Гуглстора")]
        [InfoBox("Ссылки на приложения в гуглплее", InfoMessageType.None)]
        public string[] OTHER_APP_GOOGLE_URL;

        //-----------------------------------------------------//
        [FoldoutGroup("Данные для социальных сетей")]
        [InfoBox("Издатель в фейсбуке", InfoMessageType.None)]
        public string PUBLISHER_FB = "EMPTY_URL";

        [FoldoutGroup("Данные для социальных сетей")]
        [InfoBox("Издатель в фейсбуке", InfoMessageType.None)]
        public string PUBLISHER_FB_ID = "EMPTY_URL";

        [FoldoutGroup("Данные для социальных сетей")]
        [InfoBox("Издатель в твиттере", InfoMessageType.None)]
        public string PUBLISHER_TW = "EMPTY_URL";

        [FoldoutGroup("Данные для социальных сетей")]
        [InfoBox("Разработчик в фейсбуке", InfoMessageType.None)]
        public string DEVELOPER_FB = "EMPTY_URL";

        [FoldoutGroup("Данные для социальных сетей")]
        [InfoBox("Разработчик в фейсбуке", InfoMessageType.None)]
        public string DEVELOPER_FB_ID = "EMPTY_URL";

        [FoldoutGroup("Данные для социальных сетей")]
        [InfoBox("Разработчик в твиттере", InfoMessageType.None)]
        public string DEVELOPER_TW = "EMPTY_URL";

        [FoldoutGroup("Данные для социальных сетей")]
        [InfoBox("Издатель в инстаграме", InfoMessageType.None)]
        public string PUBLISHER_INST = "EMPTY_URL";

        //-----------------------------------------------------//
        [FoldoutGroup("данные для сборки тестового билда в аппстор")]
        [InfoBox("Бандл нейм", InfoMessageType.None)]
        public string BUNDLE_IDENTIFIER_TEST = "BUNDLE_NAME";

        [FoldoutGroup("данные для сборки тестового билда в аппстор")]
        [InfoBox("ИД инапа в аппсторе", InfoMessageType.None)]
        public string IAP_NO_ADS_PRODUCT_ID_TEST = "EMPTY";

        [FoldoutGroup("данные для сборки тестового билда в аппстор")]
        [InfoBox("ИД приложения в аппсторе", InfoMessageType.None)]
        public string APPLE_ID_TEST = "EMPTY";

        [FoldoutGroup("данные для сборки тестового билда в аппстор")]
        [InfoBox("лидерборд в аппсторе", InfoMessageType.None)]
        public string APPSTORE_LEADERBOARD_TEST = "EMPTY";


        [FoldoutGroup("данные для сборки тестового билда в гуглплей")]
        [InfoBox("Бандл нейм", InfoMessageType.None)]
        public string BUNDLE_IDENTIFIER_GOOGLE_TEST = "BUNDLE_NAME";

        [FoldoutGroup("данные для сборки тестового билда в гуглплей")]
        [InfoBox("ИД инапа в гуглплее", InfoMessageType.None)]
        public string IAP_NO_ADS_PRODUCT_ID_GOOGLE_TEST = "EMPTY";

        [FoldoutGroup("данные для сборки тестового билда в гуглплей")]
        [InfoBox("ИД приложения в гуглплее", InfoMessageType.None)]
        public string GOOGLEPLAY_ID_TEST = "EMPTY";

        [FoldoutGroup("данные для сборки тестового билда в гуглплей")]
        [InfoBox("Лидерборд в гуглплее", InfoMessageType.None)]
        public string GOOPLEPLAY_LEADERBOARD_TEST = "EMPRY";
    }
}